import unittest
from maths import multiply # Cette ligne permet d'importer la fonction multiplication au sein de notre fichier de test
from maths import divide # Cette ligne permet d'importer la fonction multiplication au sein de notre fichier de test
from maths import add # Cette ligne permet d'importer la fonction multiplication au sein de notre fichier de test
from maths import substract # Cette ligne permet d'importer la fonction multiplication au sein de notre fichier de test


class TestMathsFunctions(unittest.TestCase):

    def test_multiply(self):
        self.assertEqual(multiply(2, 2), 4)
        self.assertEqual(multiply(7, 3), 21)

    def test_divide(self):
        self.assertEqual(divide(2, 2), 1)
        self.assertEqual(divide(6, 3), 2)
        with self.assertRaises(ZeroDivisionError):
            divide(6, 0)

    def test_add(self):
        self.assertEqual(add(2, 2), 4)
        self.assertEqual(add(7, 3), 10)

    def test_subtract(self):
        self.assertEqual(substract(2, 2), 0)
        self.assertEqual(substract(7, 3), 4)