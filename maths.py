def multiply(left_factor, right_factor):
    return left_factor * right_factor

def divide(left_factor, right_factor):
    if right_factor == 0:
        raise ZeroDivisionError()
    return left_factor / right_factor

def add(left_factor, right_factor):
    return left_factor + right_factor

def substract(left_factor, right_factor):
    return left_factor - right_factor